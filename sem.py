#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

import sys

from semgrammar import SemGrammar


def parse(text, *args, **kwargs):
    grammar = SemGrammar()

    #for t in grammar.tokenize(text):
    #    print t
    #print '-------------------'

    #for e in (grammar.parse(text, *args, **kwargs)):
    #    print '---- Expression:', e

    return grammar.parse_to_items(text, *args, **kwargs)


def main():
    print parse('''
{@Subject {
    #predicate @Object
}}
''', debug=False)


if __name__ == '__main__':

    main()


