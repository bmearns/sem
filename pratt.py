#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

import abc
import types


class Parser(object):
    """
    Implements a Pratt-style parser.

    Described in the following resources:
    * http://hall.org.ua/halls/wizzard/pdf/Vaughan.Pratt.TDOP.pdf
    * http://effbot.org/zone/simple-top-down-parsing.htm
    * http://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy/

    """

    def __init__(self, tokens, **kwargs):
        """
        :param tokens:  An iteratable over the tokens to be parsed.
        """
        self.debug = kwargs.pop('debug', False)
        self.__indent = 0
        self.__iter = iter(tokens)
        self.__token = None
        self.step()

    def step(self):
        """
        Returns the current `token` and steps the iterator to the next token.
        If there are no tokens left, raises
        `StopIteration`.
        """
        t = self.__token
        self.__token = self.__iter.next()
        return t

    @property
    def token(self):
        """
        The current token from the iterator, the same that will be returned by
        the next call to `step`.
        """
        return self.__token

    def advance(self, test=None, msg=None):
        """
        Similar to `step`, but performs an optional validation against the token,
        raising a `SemSyntaxError` if the validation fails.
        """
        if test is not None:
            if isinstance(test, types.StringTypes):
                test_id = test
                test = lambda t : t.token_id == test_id
                
            if not test(self.token):
                if msg is None:
                    'Unexpected token: %s' % (self.token,)
                elif isinstance(msg, types.StringTypes):
                    msg = msg % (self.token,)
                else:
                    msg = msg(self.token)
                raise self.token.syntaxError(msg)

        return self.step()

    def indent(self):
        self.__indent += 1

    def outdent(self):
        self.__indent -= 1
        assert(self.__indent >= 0)

    def dbg(self, msg):
        if self.debug:
            if not isinstance(msg, types.StringTypes):
                msg = msg()
            print ('  '*self.__indent) + msg
        
    def expression(self, rbp=0):
        """
        Parse and return a single expression, limited by the binding power given
        by ``rbp``. The ``rbp`` essentially specifies how strongly the preceding
        expression is pulling on the tokens and subexpressions.
        """
        self.dbg('Starting expression (bp=%s): %s' % (rbp, self.token,))
        self.indent()

        left = self.step().nud(self)
        self.dbg('Left is: %s' % (left,))
        self.dbg('Lbp: %s, Rbp: %s' % (left.binding_power, rbp))

        while left.binding_power > rbp:
            self.dbg('Next token (led) is: %s' % (self.token,))
            left = self.step().led(self, left)
            self.dbg('Produced left: %s' % (left,))
        self.outdent()
        return left


