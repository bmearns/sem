#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from pea import *

from sem import parse

@step
def the_input_is(inp):
    world.input = inp

@step
def there_should_be_an_exception_when_parsing(*classes):
    try:
        parse(world.input)
    except Exception, e:
        assert any(isinstance(e, clz) for clz in classes), ("Unexpected exception: %s" % e)
    else:
        assert False, "No exceptions raised."

@step
def the_input_is_parsed():
    world.items = parse(world.input)

@step
def structure_should_be_empty():
    items = world.items
    assert len(items) == 0, "Expected no items, found %d" % len(items)

@step
def there_should_be_an_item_named(name):
    items = world.items
    assert name in items, "Could not find named item: %r" % name
    assert items[name].has_id(name), "Item does not have id: %r" % name

@step
def there_should_be_a_relationship(predicate, subj, obj):
    items = world.items

    assert subj in items, "Subject not found in items: %r" % subj
    subject = items[subj]
    assert subject.has_id(subj), "Subject does not have id: %r" % subj

    objs = subject.get(predicate)
    assert any(o.has_id(obj) for o in objs), "Object not found: %r (objects for relationship are: %s)" % (obj, ', '.join(str(o) for o in objs))
    assert obj in items, "Object not found in items: %r" % obj


class TestBasicParsing(TestCase):
    def test_no_semantics(self):
        Given.the_input_is('Here is my input')
        When.the_input_is_parsed()
        Then.structure_should_be_empty()

    def test_simple_relationship(self):
        for text, subject, obj, predicate in (
                 ('{@Subject {#predicate @Object}}', 'Subject', 'Object', 'predicate'),
                 ('text {@Subject {#predicate @Object}}', 'Subject', 'Object', 'predicate'),
                 ('{text @Subject {#predicate @Object}}', 'Subject', 'Object', 'predicate'),
                 ('{@Subject text {#predicate @Object}}', 'Subject', 'Object', 'predicate'),
                 ('{@Subject {text #predicate @Object}}', 'Subject', 'Object', 'predicate'),
                 ('{@Subject {#predicate text @Object}}', 'Subject', 'Object', 'predicate'),
                 ('{@Subject {#predicate @Object text}}', 'Subject', 'Object', 'predicate'),
                 ('{@Subject {#predicate @Object} text}', 'Subject', 'Object', 'predicate'),
                 ('{@Subject {#predicate @Object}} text', 'Subject', 'Object', 'predicate'),
                 ('text { text @Subject text { text #predicate text @Object text} text } text', 'Subject', 'Object', 'predicate'),
                 ('text--0123456789.::()--text { text--0123456789.::()--text @Subject text--0123456789.::()--text { text--0123456789.::()--text #predicate text--0123456789.::()--text @Object text--0123456789.::()--text} text--0123456789.::()--text } text--0123456789.::()--text', 'Subject', 'Object', 'predicate'),
                 ('text { text @Abcdefghikl text { text #0123456798p-foo text @nOpqrstuvewxyZ text} text } text', 'Abcdefghikl', 'nOpqrstuvewxyZ', '0123456798p-foo'),
            ):
            Given.the_input_is(text)
            When.the_input_is_parsed()
            Then.there_should_be_an_item_named(subject)
            And.there_should_be_an_item_named(obj)
            And.there_should_be_a_relationship(predicate, subject, obj)
 
    def test_two_relationships(self):
        Given.the_input_is('{@MySubject {#rubs @MyObject, which {#pats @MyOtherObject}}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('rubs', 'MySubject', 'MyObject')
        And.there_should_be_a_relationship('pats', 'MyObject', 'MyOtherObject')
 
    def test_two_relationships_same_subject(self):
        Given.the_input_is('{@MySubject {#rubs @MyObject} and also {#squeezes @AnotherObject}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('rubs', 'MySubject', 'MyObject')
        And.there_should_be_a_relationship('squeezes', 'MySubject', 'AnotherObject')

    def test_identifier_namespace(self):
        Given.the_input_is('{@A {#has-method @.B}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('has-method', 'A', 'A.B')

    def test_identifier_namespace_02(self):
        Given.the_input_is('{@A {#has-method @.B} {#has-method @.C}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('has-method', 'A', 'A.B')
        And.there_should_be_a_relationship('has-method', 'A', 'A.C')

    def test_identifier_namespace_03(self):
        Given.the_input_is('{@A {#has-method @.B which {#calls @.C}}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('has-method', 'A', 'A.B')
        And.there_should_be_a_relationship('calls', 'A.B', 'A.B.C')

    def test_identifier_namespace_04(self):
        Given.the_input_is('{@A {#has-method @.B} and {#has-method @.C which {#calls @..B}}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('has-method', 'A', 'A.B')
        And.there_should_be_a_relationship('has-method', 'A', 'A.C')
        And.there_should_be_a_relationship('calls', 'A.C', 'A.B')

    def test_identifier_namespace_out_of_bounds(self):
        Given.the_input_is('{@A {#foo @.B {#bar @...C}}}')
        Then.there_should_be_an_exception_when_parsing(IndexError)

    def test_reverse_specifier(self):
        Given.the_input_is('{@MyObject is {~rubbed-by @MySubject}}.')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('rubbed-by', 'MySubject', 'MyObject')

    def test_reverse_specifier_02(self):
        Given.the_input_is('{@a {#foo @b {~bar @c}}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('foo', 'a', 'b')
        And.there_should_be_a_relationship('bar', 'c', 'b')

    def test_compound_identifier_01(self):
        Given.the_input_is('{@[@MySubject and @MyOtherSubject] both {#rub @MyObject}}.')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('rub', 'MySubject', 'MyObject')
        And.there_should_be_a_relationship('rub', 'MyOtherSubject', 'MyObject')

    def test_compound_identifier_02(self):
        Given.the_input_is('{@MySubject {#peels both @[@MyBanana and @MyApple]}}.')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('peels', 'MySubject', 'MyBanana')
        And.there_should_be_a_relationship('peels', 'MySubject', 'MyApple')
        
    def test_compound_identifier_03(self):
        Given.the_input_is('{@A {#foo @[@B @C] {#bar @[@D @E] {#baz @F}}}}')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('foo', 'A', 'B')
        And.there_should_be_a_relationship('foo', 'A', 'C')
        And.there_should_be_a_relationship('bar', 'B', 'D')
        And.there_should_be_a_relationship('bar', 'B', 'E')
        And.there_should_be_a_relationship('bar', 'C', 'D')
        And.there_should_be_a_relationship('bar', 'C', 'E')
        And.there_should_be_a_relationship('baz', 'D', 'F')
        And.there_should_be_a_relationship('baz', 'E', 'F')

    def test_identifier_delcaration(self):
        Given.the_input_is('{@MySubject {#peels @[2] both @MyBanana and @MyApple]}}.')
        When.the_input_is_parsed()
        Then.there_should_be_a_relationship('peels', 'MySubject', 'MyBanana')
        And.there_should_be_a_relationship('peels', 'MySubject', 'MyApple')
        
