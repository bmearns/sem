#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from semtoken import *
from grammar import TokenizingGrammar


class Item(object):
    def __init__(self):
        self.__props = {}

    def set(self, name, val):
        self.__props.setdefault(name, []).append(val)

    def get(self, name):
        return tuple(self.__props.get(name, []))

    def has_id(self, name):
        return name in self.__props.get('__id__')

    def get_id(self):
        ids = self.get('__id__')
        if ids:
            return ids[0]
        return None

    def __str__(self):
        return self.get_id() or ('<no-id: %s>' % (self.id))

class SemGrammar(TokenizingGrammar):
    def __init__(self):
        super(SemGrammar, self).__init__()
        self.__items = {}

    def parse_to_items(self, text, *args, **kwargs):
        for exp in self.parse(text, *args, **kwargs):
            if isinstance(exp, Block):
                self.add_block(exp)

        return self.__items

    def add_block(self, block, targets=None, path=[]):
        items = []
        npath = list(path)

        if block.targets:
            ids = block.targets
            single = len(ids) == 1

            for id in ids:
                name = id.name
                prefix_count = 0
                while name[0] == '.':
                    prefix_count += 1
                    name = name[1:]
                if prefix_count > 0:
                    if prefix_count > len(npath):
                        raise IndexError(id.syntaxError("Namespace out of scope."))
                    prefix = npath[-prefix_count]
                    if prefix is None:
                        raise IndexError(id.syntaxError("Namespace out of scope (cannot point to compound identifiers)"))
                    name = prefix + '.' + name
                    
                item = self.__items.setdefault(name, Item())
                item.set('__id__', name)
                items.append(item)

            if single:
                npath.append(name)
            else:
                npath.append(None)


        if block.specifier is not None:
            if targets is None:
                raise block.specifier.syntaxError('Cannot use specifiers in top-level scope, no parent item.')
            for target in targets:
                for item in items:
                    if block.reverse:
                        item.set(block.specifier.name, target)
                    else:
                        target.set(block.specifier.name, item)

        for b in block.nested:
            self.add_block(b, items, npath)
            

@SemGrammar.singular_token(r'\{')
class OcbToken(Token):
    id = '{'

    def __init__(self, filename, line, col):
        super(OcbToken, self).__init__(filename, line, col)
        self.contents = []

    def nud(self, parser):
        contents = []
        while not isinstance(parser.token, CcbToken):
            contents.append(parser.expression(0))
        parser.advance(test=lambda token : isinstance(token, CcbToken))
        return self.spawn(Block, contents)


class Block(Token):
    id = '<block>'
    def __init__(self, filename, line, col, contents=[]):
        super(Block, self).__init__(filename, line, col)
        self.contents = contents
        self.identifiers = []
        self.compoundIdentifier = None
        self.specifier = None
        self.reverse = False
        self.nested = []
        self.idDecl = None
        self.targets = []
        for t in contents:
            if isinstance(t, CompoundIdentifier):
                if self.compoundIdentifier is not None:
                    raise t.syntaxError('Multiple compound identifiers given in block.')
                self.compoundIdentifier = t
            elif isinstance(t, Identifier):
                self.identifiers.append(t)
            elif isinstance(t, IdentifierDeclaration):
                if self.idDecl is not None:
                    raise t.syntaxError('Multiple identifier-declarations given in block.')
                self.idDecl = t
            elif isinstance(t, Specifier):
                if self.specifier:
                    raise t.syntaxError('Multiple specifiers given in block.')
                self.specifier = t
                self.reverse = False
            elif isinstance(t, ReverseSpecifier):
                if self.specifier:
                    raise t.syntaxError('Multiple specifiers given in block.')
                self.specifier = t
                self.reverse = True
            elif isinstance(t, Block):
                self.nested.append(t)

        if self.compoundIdentifier:
            if self.idDecl is not None:
                raise self.syntaxError('Cannot mix compound-identifiers and identifier-declarations in one block.')
            elif self.identifiers:
                raise self.syntaxError('Cannot mix identifiers and compound-identifiers in one block.')
            self.targets = self.compoundIdentifier.identifiers

        elif self.idDecl is not None:
            if self.idDecl.count != len(self.identifiers):
                raise self.syntaxError('Wrong number of identifiers in block. Identifier declaration indicated %d identifiers, found %d.' % (self.idDecl.count, len(self.identifiers)))
            self.targets = self.identifiers

        elif len(self.identifiers) > 1:
            raise t.syntaxError('Multiple identifiers given in block.')

        else:
            self.targets = self.identifiers
                 
    def __str__(self):
        return '{%s}' % (', '.join(str(c) for c in self.contents))


@SemGrammar.singular_token(r'\}')
class CcbToken(Token):
    id = '}'

@SemGrammar.singular_token(r'\[')
class OsbToken(Token):
    id = '['
    def nud(self, parser):
        return self

@SemGrammar.singular_token(r'\]')
class CsbToken(Token):
    id = ']'
    def nud(self, parser):
        return self


class NameGrabberToken(Token):
    def grab_name(self, parser):
        return parser.advance(lambda t : isinstance(t, NameToken), 'Expected a name, found: %s').text

@SemGrammar.singular_token('@')
class AtToken(NameGrabberToken):
    id = '@'
    def nud(self, parser):
        if parser.token.id == '[':
            parser.advance('[')
            return self.parseSquareBrackets(parser)
        name = self.grab_name(parser)
        return self.spawn(Identifier, name)

    def parseSquareBrackets(self, parser):
        contents = []
        identifiers = []
        while not isinstance(parser.token, CsbToken):
            exp = parser.expression(0)
            if isinstance(exp, Identifier):
                identifiers.append(exp)
            elif not isinstance(exp, TextToken):
                raise self.syntaxError('Illegal content inside compound identifier: %s' % (exp,))
            contents.append(exp)
        parser.advance(test=lambda token : isinstance(token, CsbToken))

        if len(identifiers) == 0 and len(contents) == 1 and isinstance(contents[0], TextToken):
            try:
                number = int(contents[0].text)
            except ValueError:
                pass
            else:
                return self.spawn(IdentifierDeclaration, number)

        return self.spawn(CompoundIdentifier, contents, identifiers)

class IdentifierDeclaration(Token):
    id = '<id-decl>'
    def __init__(self, filename, line, col, count):
        super(IdentifierDeclaration, self).__init__(filename, line, col)
        self.count = count

class CompoundIdentifier(Token):
    id='<comp-id>'

    def __init__(self, filename, line, col, contents, identifiers):
        super(CompoundIdentifier, self).__init__(filename, line, col)
        self.contents = contents
        self.identifiers = identifiers


@SemGrammar.singular_token('#')
class PoundToken(NameGrabberToken):
    id = '#'
    def nud(self, parser):
        name = self.grab_name(parser)
        return self.spawn(Specifier, name)

@SemGrammar.singular_token('~')
class TildeToken(NameGrabberToken):
    id = '~'
    def nud(self, parser):
        name = self.grab_name(parser)
        return self.spawn(ReverseSpecifier, name)


class NamedItem(Token):
    def __init__(self, filename, line, col, name):
        super(NamedItem, self).__init__(filename, line, col)
        self.name = name

class Identifier(NamedItem):
    id = '<id>'
    def __str__(self):
        return '@%s' % self.name

class Specifier(NamedItem):
    id = '<spec>'
    def __str__(self):
        return '#%s' % self.name

class ReverseSpecifier(NamedItem):
    id = '<rev-spec>'
    def __str__(self):
        return '~%s' % self.name


@SemGrammar.token('\n')
class LnbrkToken(CaptureToken):
    id = 'lnbrk'
    def nud(self, parser):
        return self


class TextToken(CaptureToken):
    id = 'text'
    def nud(self, parser):
        return self

@SemGrammar.token(r'\s+')
class WhitespaceToken(TextToken):
    id = 'whitespace'

@SemGrammar.token(r'[-_\.0-9a-zA-Z]*[-_0-9a-zA-Z]+')
class NameToken(TextToken):
    id = 'name'

SemGrammar.token(r'[^\s\@\#\{\}\[\]~]+')(TextToken)

