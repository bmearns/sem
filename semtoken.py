#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

import abc

class SemSyntaxError(SyntaxError):
    def __init__(self, message, token=None):
        super(SemSyntaxError, self).__init__(message)
        self.token = token

        #Inherited data properties from SyntaxError
        if token is not None:
            self.filename = token.filename
            self.lineno = token.line
            self.offset = token.col


class AbstractToken(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def binding_power(self):
        """
        The left binding power of the token.
        """
        raise NotImplementedError()

    @abc.abstractproperty
    def token_id(self):
        """
        Uniquely identifies the type of the token, usually as a string.
        """
        raise NotImplementedError()

    def __init__(self, filename, line, col):
        self.filename = filename
        self.line = line
        self.col = col

    def spawn(self, cls, *args, **kwargs):
        return cls(self.filename, self.line, self.col, *args, **kwargs)

    def syntaxError(self, message=None):
        return SemSyntaxError(message, self)

    def __str__(self):
        return self.token_id or '<no-id>'

    def nud(self, parser):
        raise self.syntaxError("Unexpected token: %s (did you forget to implement the nud method?)" % (self,))

    def led(self, parser, left):
        raise self.syntaxError("Unexpected token: %s (did you forget to implement the led method?)" % (self,))


class Token(AbstractToken):
    id = None
    lbp = 0

    @property
    def binding_power(self):
        return self.lbp

    @property
    def token_id(self):
        return self.id

class CaptureToken(Token):
    """
    A `Token` which stores the associated text in the `text` attribute.
    """
    def __init__(self, filename, line, col, text=None):
        super(CaptureToken, self).__init__(filename, line, col)
        self.text = text

    def __str__(self):
        return '%s(%r)' % (self.token_id, self.text)

class EndToken(Token):
    id = '(end)'

