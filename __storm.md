
This should be pretty straight forward. It's all built on **triplets**, also
called **relationships**. A triplet consists of 3 items: a _subject_, a
_predicate_ and an _object_. The **predicate** specifies a particular
(directed) relationship from the subject to the object. Objects may themselves
have arbitrary predicates linking them to other items, creating a complex
directed graph where the edges are predicates and nodes are subjects
and/or objects.

The document includes a tree of nested **blocks** contained by curly braces.
These blocks (and the rest of the document) can contain nearly arbitrary
content, referred to as **unadorned content**, which has so specific semantic
meaning. In addition, they can contain a variety of _adorned content_ which
does have special meaning. Key among this adorned content are _identifiers_
and _specifiers_.

A block refers to a specific item (or collection of items). It may also define
a relationship between this block's item and the parent block's item.

The following defines a relationship using two blocks:

    {@MySubject {#rubs @MyObject}}
    
The following defines two relationships amongst three items, using three
blocks:

    {@MySubject {#rubs @MyObject, which {#pats @MyOtherObject}}}

The following also defined two relationships, but the subject is the same for
both of them, only the object is different:

    {@MySubject {#rubs @MyObject} and also {#squeezes @AnotherObject}}
    
Notice the unadorned content in the previous two examples: "which" in the
first and "and also" in the second. These have no special meaning, they are
effectively ignored by the semantic processor. They are just there to make it
more readable to a person. This is a key aspect of the language.

Likewise, the order of the _identifier_ (to identify the target item) and the
_specifier_ (to identify the predicate) doesn't matter as long as they both
appear in the block.

In addition to standard forward specifiers, you can use **reverse specifiers**
to turn the relationship around, so that it points from the inner item to the
outer:

    {@MyObject is {~rubbed-by @MySubject}}.

Identifiers and relationships follow simple namespacing rules, where each
block creates a namespace and the `.` character is used to transgress them.
Each `.` at the beginning of the token goes up one block, the remainder of the
token is joined to the end of the identifier in the selected block, with
another `.`.

    {@A {#has-method @.B} and {#has-method @.C which {#calls @..B}}
    
This generates items named `A`, `A.B`, and `A.C` and the following
relationships:
    
    * A (has-method) A.B
    * A (has-method) A.C
    * A.C (calls) A.B

You can supply multiple targets in a block, but because it is often a mistake
when two identifiers appear in the same block, they need to be explicitly
grouped using a **compound identifier**:


    {@[@MySubject and @MyOtherSubject] both {#rub @MyObject}}.
    {@MySubject {#peels both @[@MyBanana and @MyApple]}}.

The first line specifies two subjects that both have the `rub` reltionship
with the same object. The second line specifies that a single subject has the
`peel` relationship with two different objects. Note that in the second case,
all items specified in the compound identifier become the targets of the
block, which means any nested relationships apply to all of them.

Alternatively, you can use an **identifier declaration** to specifiy how many
identifiers are expected in the block, and then each identifier can be
specified on their own but are treated as a compound identifier:

XXX: Left off here

    {@[2] @MySubject {#rubs #MyObject}. So does @MyOtherSubject}.

If no identifier appears in a block, then it identifies an **anonymous item**.
You can use nested relationships inside such a block, but you won't be able to
refer (directly) to the item anywhere outside the block that defines it.

    {@MySubject {#rubs something which {#is-colored @red} and is {@small in #size}}}.
    
The above example says that the subject has the `rub` relationship with some
unnamed _anonymous_ item, and that item is small and red.

If a relationship is not specified in a block, then it simply creates a new
scope, where the identifier specified in the block (or an anonymous item if no
identifier is specified) is the implicit target for nested specifiers.

In some cases, the object of a relationship is not actually an item, but just
a simple **primitive** value:

    {@MySubject {is #named $Paul}}

All primitives are captured as strings, it is up to post-processing to
determine if they should be a different type.

Sometimes, an _identifier_ or _specifier_ (or even a _literal_) doesn't fit
well into the prose you are writing. In this case, you can **pronounce** the
token one-way for your literature and **whisper** it differently for the
semantic processor:

    {@<Anna|annabelle> { #<likes to eat|favorite-food> @<Bananas|banana>}}.

The content on the left side of the bar is _pronounced_ in the literature,
and the right hand side is _whispered_ for the semantic processor. If no bar
is given, the entire content is whispered and nothing is pronounced.

    `eat<s|>` --> pronounced "eats", whispered as "eat"

    `eat<en|s>` --> pronounced "eaten", whispered as "eats"

    `<eat>` --> _not_ pronounced, whispered as "eat"

These constructs can contain spaces and other special characters, but angle
brackets, veritcal bars, and backslashes need to be escaped with a leading
backslash. For this reason, they are referred to as **extended names**.
    
    `<eat \> food|eat-more-food>` --> pronounced as "eat > food", whispered as
        "eat-more-food"

_Extended names can be combined arbitrarily with other extended or
non-extended names as long as they directly connect:

    `@foo<Bar|bar>Trot<Burger>`

Will be pronounced as "fooBarTrot" in the literature, but whispered as
"foobarTrotBurger" to the semantic processor.

Extended names only apply as a name (or part of a name). Otherwise, singular
angle brackets and singular angle bracket pairs have no special meaning and
are not parsed or processed differently.

If an extended name contains exactly two bars and they appear as the last
characters in the whisper, then it is pronounced and whispered the same way.

    `<eat more food||>` --> pronouned _and_ whispered as "eat more food"

Unescaped characters on the whispered side of the pipe are interpretted the
same as any other name. This is useful for whispering compound identifiers:

    @<They|[@He and @She]>

The above example is pronounced "They", but whispers a compound identifier
containing `@He` and `@She` for the semantic processor.

Double angle brackets are used to denote **literals**, which are a more
general form of escaping, specifically escaping characters that would
otherwise have semantic relevance. Angle brackets and
backslashes must be escaped with a backslash inside a literal, but
they can otherwise contain any characters without any special meaning.

    An comparison of two identifiers might look
    like <<@MyEyes \> @MyStomach>>.
    
There is a simpler escaping mechanism for prose as well. To use a `@`, `#`,
`~`, `{`, or `}` character as prose, you can prefix it with a `@` character.
Not all of these things always need to be escaped, depending on the context,
but it is always safe to do so. These are the _only_ characters that can be
escaped this way.

Tripled angle brackets are used for _extended literals_. An **extended
literal** starts on the line immediately following the opening brackets, and
continues until the closing brackets appear alone and at the very beginning of
a line. Nothing can (or needs to be) escaped inside the extended literal, but
nothing except the closing brackets has any special meaning. Even other
occurrences of triple closing angle brackets (not along on a line, or not at
the very start of the line) can appear:

    The following is just prose: <<<
    This and @This and even <<<this>>>
    or this...
     >>>
    or this...
    >>> because it's not alone.
    >>>

There are additional variants of this form which have different purposes and
processing, based on the characters that follow the opening triple brackets.

If the first character is a `%`, then the entire block is a **comment**, and ends
at the first occurrence of the triple closing brackets (i.e., it is not the
block form). _Comments_ are not included in the literature and are ignored by
the semantic processor. They are effectively empty tokens.

    Here is some prose<<<%, here is a comment>>>, and here is some more
    prose.

If the first characters are a pair of dashes, it also creates a _comment_ but in
this case it _does_ use the block form, so it is not ended until the triple
closing brackets appear alone and at the start of a line Optionally, the
closing brackets can be preceeded by a pair of dashes as well. This is called a
**block comment**.

If the first character is alphanumeric, it creates a **super-extended
literal**, which is similar to HEREDOC syntax in some other languages.
The string of alphanumeric characters that immediately follows the opening
brackets defines the **tag** for the block. The block is then terminated when
the _tag_ appears at the beginning of the line immediately followed by the
triple closing brackets. Other than the parsing, it acts like an _extended
literal_. Tags can likewise be used for _block comments_, in which case they
are given after the pair of dashes, and must appear at the start of a line,
followed immediately by the closing brackets with optional leading dashes.

    <<<--Foo
    This is a block comment which won't end until You see "Foo>>>" or "Foo-->>>"
    alone and at the start of a line.
    Foo-->>>

Any other non-whitspace character mmediately following the opening brackets is
reserved for future use, and may cause an error or warning. 

Literals and extended literals can be used as tokens, just like _extended
tokens_.

A **reference** is used in your prose to refer to an item without actually
affecting the semantic processor. It can appear anywhere that prose can
appear.

    This might generate a link to &MyIdentifier, for instance.
    
References follow the same naming semantics as identifiers, so they can use
namespacing.

You can define **aliases** for identifiers as:

    &identifier=alias
    
Aliases follow the same rules as identifiers, so they are universal, but can
also use namespacing. Universal aliases can be tricky, because an alias
defined anywhere in the document applies everywhere. Universal aliases are
best for unique nick names. Other aliases, such as pro-nounds (he, she, it,
etc), should be namespaced.

Aliases can be chained to assign multiple aliases to the same identifier.

    &identifier=alias1=alias2
    
Aliases can be assigned directly on identifiers as well:

    @identifier=alias

Aliases are never pronounced.





<!---
 vim: set tw=78:
-->

