#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from semtoken import EndToken, SemSyntaxError
from pratt import Parser
import re

class Grammar(object):

    def parse_tokens(self, tokens, *parse_args, **kwargs):
        """
        Parses the given iterable of tokens by instantiating a new
        `Parser` and yielding each `~Parser.expression` one at a time.
        """
        p = Parser(tokens, *parse_args, **kwargs)
        while True:
            try:
                yield p.expression(0)
            except StopIteration:
                raise SemSyntaxError("Program ended in the middle of an expression.")

            if isinstance(p.token, EndToken):
                break

class TokenizingGrammar(Grammar):
    """
    A simple extension to `Grammar` that does simple tokenization using regular
    expressions.
    """

    __token_specs = []

    @classmethod
    def add_token_factory(cls, pattern, factory):
        """
        Add a token factory for tokenizing.

        :param str pattern:         A regex pattern that is used for identifying the token.
        :param callable factory:    The factory function that will be called to instantiate
            tokens when the given pattern matches. The function will be called with the
            following arguments: :samp:`({filename}, {linenum}, {column}, {token})`
            The first three specify where the token was found, the :samp:`{token}` argument
            specifies is the string that matched the pattern.
            Note that you can use a class object itself as
            a factory, as long as it supports the correct arguments.
        """
        cls.__token_specs.append((pattern, factory))

    @classmethod
    def add_singular_token_factory(cls, pattern, factory):
        """
        Like `add_token_factory`, except factory function does not take the
        token value, it only takes the filename, linenum, and column arguments.
        """
        cls.add_token_factory(pattern, lambda fn, ln, co, t : factory(fn, ln, co))

    @classmethod
    def token(cls, pattern):
        """
        Returns a class decorator which adds the decorated class as a token factory
        for the given pattern, with `add_token_factory`.
        """
        def decorator(c):
            cls.add_token_factory(pattern, c)
            return c
        return decorator

    @classmethod
    def singular_token(cls, pattern):
        """
        Returns a class decorator which adds the decorated class as a token factory
        for the given pattern, with `add_singular_token_factory`.
        """
        def decorator(c):
            cls.add_singular_token_factory(pattern, c)
            return c
        return decorator


    def parse(self, text, *parser_args, **kwargs):
        """
        Parses the given text using `tokenize` to break the text into tokens and delegating
        to `~Grammar.parse_tokens`.
        """
        for e in self.parse_tokens(self.tokenize(text), *parser_args, **kwargs):
            yield e

    def tokenize(self, text, filename=None, line=1):
        """
        Breaks the given text into a sequence of tokens according to the current
        sequence of token specs, yielding each token in turn.
        """

        token_regex = re.compile('|'.join('(%s)' % spec[0] for spec in self.__token_specs))

        pos = 0
        line_start = 0
        mobj = token_regex.match(text)
        while mobj:
            gidx = mobj.lastindex
            col = mobj.start() - line_start
            token = self.__token_specs[gidx-1][1](filename, line, col, mobj.group(gidx))

            yield token

            if token.id == 'lnbrk':
                line_start = pos
                line += 1

            pos = mobj.end()
            mobj = token_regex.match(text, pos)

        if pos != len(text):
            snippet = text[pos:pos+10]
            if len(snippet) == 10:
                snippet = snippet[:9] + '...'
            raise SemSyntaxError('Could not parse token on line %d (column %d): %r' % (line, pos - line_start, snippet))

        yield EndToken(filename, line, pos-line_start)
            

